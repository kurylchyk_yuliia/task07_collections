package com.binary_tree;

import java.awt.print.Printable;
import java.util.Map;
import java.util.Scanner;

public class Model {

    BinaryTree binaryTree;
    private Scanner sc;
    private Map<String, Printable> methodsMenu;


    Model(BinaryTree binaryTree) {

        this.binaryTree  = binaryTree;

    }

    /**
     * Shows the size of Binary Tree.
     * @return the size
     */
    public int sizeOfBinaryTree() {
        return binaryTree.size();
    }

    /**
     * Shows if binary tree contains the key
     * @param key
     * @param <K>
     * @return true if it contains
     */
    public <K extends Comparable<K>> String containsKey(K key) {

        if(binaryTree.containsKey(key)) {
         return "It contains the key "+ key;
        } else {
            return "It does not contain the key "+ key;
        }
    }

    /**
     * Shows if binary tree contains the value
     * @param value
     * @param <V>
     * @return true if it contains
     */
    public <V> String containsValue(V value) {

        if(binaryTree.containsKey(value)) {
            return "It contains the value "+ value;
        } else {
            return "It contains the value "+ value;
        }
    }

    /**
     * shows the value by key
     * @param key
     * @return the value
     */

    public String getValueByKey(Object key) {
        if(binaryTree.get(key)==null) {
            return "there is no key "+key;
        } else {
            return "The value - "+binaryTree.get(key).toString();
        }
    }

    /**
     * Adding the element to the tree
     * @param key
     * @param value
     * @param <K>
     * @param <V>
     * @return the added value
     */
    public <K extends Comparable<K>,V > String putTheElement(K key, V value) {
       return "The added value " + binaryTree.put(key,value).toString();
    }

    /**
     * removing the element
     * @param key
     * @param <K>
     * @param <V>
     * @return
     */
    public <K extends Comparable<K>,V > String removeTheElement(K key) {

        return "removed element "  + binaryTree.remove(key).toString();
    }

    public String showAll(){
        return binaryTree.print();
    }

    public void clear(){

        binaryTree.clear();
    }
}
