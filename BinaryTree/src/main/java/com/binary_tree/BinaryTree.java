package com.binary_tree;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;


public class BinaryTree<K extends Comparable<K>, V> implements Map<K, V> {

    private Node root;
    private int size;
    private ArrayList<K> keys;


    BinaryTree() {
        root = new Node<K, V>();
        size = 0;
        keys = new ArrayList<K>();
    }

    /**
     * shows the count of elements in binary tree
     *
     * @return the size of element
     */
    public int size() {
        return size;
    }

    /**
     * shows whether the binary tree is empty
     *
     * @return true is its empty
     */
    public boolean isEmpty() {
        if (size == 0) {
            return true;
        } else return false;
    }

    /**
     * shows if there is appropriate key
     *
     * @param key
     * @return true if the key is present
     */
    public boolean containsKey(Object key) {
        Node<K, V> temp = root;
        while (true) {

            if (temp == null) {
                return false;
            } else if (temp.key.compareTo((K) key) == 0) {
                return true;
            } else if (temp.key.compareTo((K) key) < 0) {
                temp = temp.right;
            } else if (temp.key.compareTo((K) key) > 0) {
                temp = temp.left;
            }
        }


    }

    /**
     * show if binary tree has appropriate value
     *
     * @param value
     * @return
     */
    public boolean containsValue(Object value) {
        Node<K, V> temp = root;
        int index = 0;
        K key;
        while (index < keys.size()) {
            key = keys.get(index);
            if (temp == null) {
                return false;
            } else if (temp.key.compareTo((K) key) == 0) {
                if (temp.getValue() == (V) value) {
                    return true;
                } else {
                    index++;
                    temp = root;
                    continue;
                }
            } else if (temp.key.compareTo((K) key) < 0) {
                temp = temp.right;
            } else if (temp.key.compareTo((K) key) > 0) {
                temp = temp.left;
            }
        }
        return false;
    }

    /**
     * finds the value using the key
     *
     * @param key
     * @return the value
     */
    public V get(Object key) {
        Node<K, V> temp = root;
        while (true) {

            if (temp == null) {
                //add logger
                return null;
            } else if (key == temp.key) {
                return temp.getValue();
            } else if (temp.key.compareTo((K) key) > 0) {
                temp = temp.left;
            } else {
                temp = temp.right;
            }
        }
    }

    /**
     * putting the element into the binary tree
     *
     * @param key
     * @param value
     * @return added of refreshed element
     */
    public V put(K key, V value) {
        if (size == 0) {
            root = new Node(key, value);
            size++;
            keys.add(key);
            return (V) root.getValue();
        } else {
            Node<K, V> inserted = new Node<K, V>(key, value);
            Node<K, V> temp = root;

            while (true) {

                if (temp.getKey().compareTo(inserted.getKey()) < 0) {

                    if (temp.getKey() != key) {
                        if (temp.right == null) {
                            temp.right = inserted;
                            size++;
                            keys.add(key);
                            break;
                        } else {
                            temp = temp.right;
                        }
                    } else {
                        temp.value = value;
                    }
                } else {

                    if (temp.getKey() != key) {
                        if (temp.left == null) {
                            temp.left = inserted;
                            size++;
                            keys.add(key);
                            break;
                        } else {
                            temp = temp.left;
                        }
                    } else {
                        temp.value = value;
                        break;
                    }
                }
            }
            return inserted.getValue();
        }

    }

    private Node getNode(K k) {

        Node<K, V> temp = root;
        while (true) {

            if (temp == null) {
                return null;
            } else if (temp.key.compareTo((K) k) == 0) {
                return temp;
            } else if (temp.key.compareTo((K) k) < 0) {
                temp = temp.right;
            } else if (temp.key.compareTo((K) k) > 0) {
                temp = temp.left;
            }
        }

    }

    /**
     * removing the element using key
     * @param key
     * @return
     */
    public V remove(Object key) {
        Node<K, V> temp = root;
        Node<K, V> removedNode;
        while (true) {

            if (key == temp.key) {

                if (temp.right == null && temp.left == null) {

                    removedNode = temp;
                    temp = null;
                    return removedNode.getValue();
                }
                if (temp.left == null || temp.right == null) {

                    removedNode = temp;
                    if (temp.left == null) {

                        temp.parent.right = temp.right;
                    } else {
                        temp.parent.left = temp.left;
                    }
                    return removedNode.getValue();
                }
                if (temp.left != null && temp.right != null) {


                    Node<K, V> parentWith2Children = temp;
                    removedNode = parentWith2Children;
                    Node<K,V> leftTemp = parentWith2Children.left; // left children of element to delete
                    Node<K,V> rightTemp = parentWith2Children.right; // right children of element to delete
                    Node<K, V> theLeft = parentWith2Children.right; // finding the most left element
                    while (theLeft.left != null) {
                        theLeft = theLeft.left;
                    }

                    if(parentWith2Children.parent != null) {

                        if(parentWith2Children.parent.getKey().compareTo(parentWith2Children.getKey())<0)
                        {
                            parentWith2Children.parent.right = theLeft;
                        } else {
                            parentWith2Children.parent.left = theLeft;
                        }

                        parentWith2Children = theLeft;
                        theLeft.left = leftTemp;
                        theLeft.right  =rightTemp;

                    } else {

                        parentWith2Children = theLeft;
                        parentWith2Children.right = rightTemp;
                        parentWith2Children.left = leftTemp;
                    }


                    return removedNode.getValue();
                }

            } else if (temp.key.compareTo((K) key) > 0) {
                Node<K, V> saveParent = temp;
                temp = temp.left;
                temp.parent = saveParent;
            } else {
                Node<K, V> saveParent = temp;
                temp = temp.right;
                temp.parent = saveParent;
            }
        }

    }

    public String print() {
        int index = 0;
        String allElement ="";
        K key;
        Node<K,V> temp = root;
        while (index < keys.size()) {
            key = keys.get(index);
            if (index == 0) {
               allElement+="Root:\n";
               allElement+=root.getKey().toString()+"\t" + root.getValue().toString() +"\n";
            } else {

                temp = getNode(key);
                if(temp!=null) {
                   allElement+= temp.getKey() + "\t" + temp.getValue()+"\n";
                }
            }
            index++;
        }

        return allElement;
    }

    public void putAll(Map<? extends K, ? extends V> m) { }
    /**
     * clear binary tree
     */
    public void clear() {
        root.value = null;
        root.key = null;
        root.left = root.right = null;
        size = 0;
        root = null;
    }

    public Set<K> keySet() {
        return null;
    }

    public Collection<V> values() {
        return null;
    }

    public Set<Entry<K, V>> entrySet() {
        return null;
    }

    private class Node<K extends Comparable<K>, V> {

        private Node<K, V> parent;
        private Node<K, V> left, right;
        private K key;
        private V value;

        Node() {

            this.key = null;
            this.value = null;
            left = right = null;
            parent = null;

        }

        Node(K key, V value) {
            this.key = key;
            this.value = value;
            left = right = null;
            parent = null;
        }


        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }


    }


}
