package com.binary_tree;

public class Controller {
   static View view = new View();
   static Model model;
    public static void main(String[] args) {

        BinaryTree<Integer, Character> binaryTree = new BinaryTree<Integer, Character>();
        model = new Model(binaryTree);

       view.show(model.putTheElement(5,"S"));
        view.show(model.putTheElement(4,"F"));
        view.show(model.putTheElement(6,"K"));
        view.show(model.putTheElement(7,"I"));
        view.show(model.putTheElement(1,"O"));
        view.show(model.putTheElement(3,"T"));
        view.show(model.putTheElement(8,"E"));
        view.show(model.removeTheElement(6));
        view.show(model.containsKey(6));
        view.show(model.showAll());


    }
}
