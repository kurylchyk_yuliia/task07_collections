package com.two_menus;

@FunctionalInterface
public interface Printable {

    void print();
}
