package com.two_menus;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.*;
/**
 * Class which shows the menu and do some operations
 */
public class ViewMap {
    /**
     * menuMap contains the full menu
     */
    private Map<Integer, String> menuMap;
    /**
     * methodMap contains the all functions
     */
    private Map<Integer, Printable> methodMenu;
    /**
     * sc reads the user's input
     */
    private static Scanner sc = new Scanner(System.in);


    /**
     * Logger - obj which works with log4j
     */
    private static Logger logger1 = LogManager.getLogger(ViewEnum.class);

    /**
     * Constructor initialize all the variables
     */
    public ViewMap() {
        menuMap = new LinkedHashMap<Integer, String>();
        methodMenu = new LinkedHashMap<Integer, Printable>();

        menuMap.put(1, "Print hello world");
        menuMap.put(2, "Show the date");
        menuMap.put(3, "Show the day of week");
        menuMap.put(4, "Do calculations");
        menuMap.put(5, "Exit");

        methodMenu.put(1, this::printHello);
        methodMenu.put(2, this::printDate);
        methodMenu.put(3, this::printTheDay);
        methodMenu.put(4, this::MathOperation);
    }

    /**
     * prints hello world
     */
    private void printHello() {
        logger1.info("Hello world!");
    }

    /**
     * prints the date
     */
    private void printDate() {
        Date date = new Date();
        logger1.info(new SimpleDateFormat("yyyy.MM.dd  HH:mm").format(date));
    }

    /**
     * prints the day of week
     */
    private void printTheDay() {

        Date now = new Date();

        SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE"); // the day of the week abbreviated
        logger1.info(simpleDateformat.format(now));
    }

    /**
     * shows all the menu
     */
    private void showMenu() {
        int count = 0;
        for (String element : menuMap.values()) {
            System.out.println(++count + "\t" + element);
        }
    }

    /**
     * shows all the menu and user have to choose the option
     */
    public void show() {
        Integer keyMenu;
        do {
            showMenu();
            System.out.println("Select appropriate option");
            keyMenu = sc.nextInt();

            try {
                methodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }

        } while (keyMenu != 5);
    }

    /**
     * Do basic operations using enum methods
     */
    public void MathOperation() {
        Scanner getStr = new Scanner(System.in);
        Scanner getDouble= new Scanner(System.in);
        double x = 0;
        double y = 0;
        System.out.println("Choose the option:");
        String key;
        for (ViewEnum view : ViewEnum.values()) {
            System.out.print(view + "\t");
        }
        System.out.println("");
        key = getStr.nextLine().toUpperCase();

       ViewEnum action = ViewEnum.valueOf(key);
        System.out.println("Enter the x:");
        x = getDouble.nextDouble();
        System.out.println("Enter the y:");
        y = getDouble.nextDouble();

        logger1.info("Result\t" + action.action(x,y));


    }
}

