package com.two_menus;

/**
 * Enum with different constants which provide user to do some math operations
 */
public enum ViewEnum {
    SUM {
        public double action(double x, double y) {
            return x + y;
        }
    },

    SUBTRACT {
        public double action(double x, double y) {
            return x - y;
        }
    },

    MULTIPLY {
        public double action(double x, double y) {
            return x * y;
        }

    },
    DIVIDE {
        public double action(double x, double y) {
            return x / y;
        }
    };

    public abstract double action(double x, double y);
}

